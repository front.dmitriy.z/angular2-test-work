import { Component } from '@angular/core';

import '../assets/styles/entry.scss';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent {}
